# ---- Base Node ---- #
FROM ubuntu:14.04 AS base
RUN apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 8842ce5e && \
    echo "deb http://ppa.launchpad.net/bitcoin/bitcoin/ubuntu trusty main" > /etc/apt/sources.list.d/bitcoin.list
RUN apt-get update && \
    apt-get install -y build-essential libtool autotools-dev autoconf libssl-dev libqrencode-dev git-core libevent-dev libboost-all-dev libdb++-dev libminiupnpc-dev pkg-config && \
    apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

# ---- Build Source ---- #
FROM base as build
RUN git clone https://github.com/leafcoin/leafcoin.git /opt/leafcoin && \
    cd /opt/leafcoin/src && \
    make -j2 -f makefile.unix

# ---- Release ---- #
FROM ubuntu:14.04 AS release
RUN apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 8842ce5e && \
    echo "deb http://ppa.launchpad.net/bitcoin/bitcoin/ubuntu trusty main" > /etc/apt/sources.list.d/bitcoin.list
RUN apt-get update && \
    apt-get install -y  libboost-all-dev libdb4.8 libdb4.8++ libminiupnpc-dev libdb++-dev && \
    apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*
RUN groupadd -r leafcoin && useradd -r -m -g leafcoin leafcoin
RUN mkdir /data
RUN chown leafcoin:leafcoin /data
COPY --from=build /opt/leafcoin/src/leafcoind /usr/local/bin/
USER leafcoin
VOLUME /data
EXPOSE 22813 22812
CMD ["/usr/local/bin/leafcoind", "-datadir=/data", "-conf=/data/leafcoin.conf", "-server", "-txindex", "-printtoconsole"]